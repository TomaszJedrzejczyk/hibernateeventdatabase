package database;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateConfiguration {

    private static EntityManagerFactory factory;

    static {
        factory = Persistence.createEntityManagerFactory("sda11");
        // blok statyczny wywołany jest tylko razy, przy tworzeniu obiektu podczas uruchamiania programu
    }

    public static EntityManager getEntityMenager() {
        return factory.createEntityManager();
    }

    public static void close() {
        factory.close();
    }
}
