package database;

import domain.Event;
import domain.User;
import domain.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

public class HibernateUserRepository implements UserRepository {

    public User createNewUser(User user) { //tworzy nowego użytkownika
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();

        return user;
    }

    @Override //wyszukuje użytkownika po podanym adresie email
    public User findUserByEmail(String email) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        TypedQuery<User> query = entityManager.createQuery("from User where email = :email_param", User.class);
        query.setParameter("email_param", email);
        User user = query.getResultStream().findFirst().get();
        entityManager.close();

        return user;
    }

    @Override // metoda zwraca listę 'events' których
    public List<Event> findAllEventsByUser(User user) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        user = entityManager.getReference(User.class, user.getId());
        List<Event> userEvents = user.getEvents();
        userEvents.size();
        entityManager.close();

        return userEvents;
    }
}
