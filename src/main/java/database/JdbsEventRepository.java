package database;

import domain.Event;
import domain.EventRepository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static database.JdbcConnection.*;

public class JdbsEventRepository implements EventRepository {

    public List<Event> findAllEvents() {
        List<Event> events = new ArrayList<>();
        String selectQuery = "SELECT * FROM event LEFT JOIN location l on event.location_id = l.id;"; // zapytanie do bazy danych

        try (Connection connection = getConnection();
             Statement statement = connection.createStatement(); // przygotowanei obiektu dla zapytania
             ResultSet resultSet = statement.executeQuery(selectQuery)) { // wykonanie zapytania przez obiekt statemant

            while (resultSet.next()) { //jest to tak jakby wskaznik ktory idzie po kazdy wierszu i sprawdza true/false
                Event event = new Event(); // dla kazdego z eventu pobieramy z set z naszej bazy danych
                event.setId(resultSet.getLong("id"));
                event.setName(resultSet.getString("name"));
                event.setNumberOfSeats(resultSet.getInt("number_of_seats"));
                event.setTicketPrice(resultSet.getDouble("ticket_price"));
                event.setDate(resultSet.getTimestamp("date_time").toLocalDateTime());
                events.add(event);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Event createNewEvent(Event event) {
        return null;
    }

    @Override
    public Event findEventById(Long id) {
        return null;
    }

    @Override
    public Event updateExistingEvent(Event event) {
        return null;
    }

    @Override
    public void deleteExistingEvent(Long eventId) {

    }
}


