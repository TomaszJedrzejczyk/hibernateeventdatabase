package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcConnection {

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/sda11?useSSL=false&serverTimezone=Europe/Warsaw";
        Properties properties = new Properties(); //
        properties.put("user", "root");
        properties.put("password", "root");

        return DriverManager.getConnection(url, properties);
    }
}