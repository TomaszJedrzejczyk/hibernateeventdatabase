package database;

import domain.EventRepository;
import domain.LocationRepository;
import domain.UserRepository;

public class DataBaseFacade {

    public static UserRepository getUserRepository() {
        return new HibernateUserRepository();
    }

    public static EventRepository getEventRepository() {
        return new HibernateEventRepository();
    }

    public static LocationRepository getLocationRepository() {
        return new HibernateLocationRepository();
    }
}
