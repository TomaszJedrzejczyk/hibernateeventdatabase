package database;

import domain.Event;
import domain.EventRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import java.util.List;

public class HibernateEventRepository implements EventRepository {

    @Override // wyszukuje wszystkie 'eventy' w bazie danych
    public List<Event> findAllEvents() {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();

        return entityManager.createQuery("FROM Event", Event.class).getResultList();
    }

    @Override // zapisuje nowy obiekt 'event' do bazy danych
    public Event createNewEvent(Event event) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(event);
        transaction.commit();

        return event;
    }

    @Override // wyszukuje 'event' po jego nr id
    public Event findEventById(Long id) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();

        return entityManager.find(Event.class, id, LockModeType.PESSIMISTIC_WRITE);
    }

    @Override // aktualizuje już istniejący 'event' w bazie danych
    public Event updateExistingEvent(Event event) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(event);
        transaction.commit();

        return event;
    }

    @Override //usuwa istniejacy 'event' z bazy danych
    public void deleteExistingEvent(Long eventId) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Event event = entityManager.getReference(Event.class, eventId);
        entityManager.remove(event);
        transaction.commit();
    }
}
