package database;

import domain.Event;
import domain.Location;
import domain.LocationRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class HibernateLocationRepository implements LocationRepository {

    @Override //szuka lokalizacji po podanym id
    public Location findLocationById(Long id) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();

        return entityManager.find(Location.class, id);
    }

    @Override //dostarcza liste wszystkich 'events' z podaje lokalizacji
    public List<Event> findListOfEventsByLocation(Location location) {
        EntityManager entityManager = HibernateConfiguration.getEntityMenager();
        location = entityManager.getReference(Location.class, location.getId());

        return location.getEvents();
    }
}
