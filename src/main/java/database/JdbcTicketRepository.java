package database;

import domain.Ticket;

import java.sql.*;

import static database.JdbcConnection.getConnection;

public class JdbcTicketRepository {

    public Ticket buyTicket(Ticket ticket) {
        String insertQuery = "INSERT INTO ticket (event_id, user_id, count) VALUES (?,?,?)";

        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {

            statement.setInt(3, ticket.getSeats());
            statement.setLong(1, ticket.getEvent().getId());
            statement.setLong(2, ticket.getUser().getId());

            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            ticket.setId(resultSet.getLong(1));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ticket;
    }
}
