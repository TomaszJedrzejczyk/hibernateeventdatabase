package domain;

import java.util.List;

public interface EventRepository {

    public List<Event> findAllEvents();

    public Event createNewEvent(Event event);

    public Event findEventById(Long id);

    public Event updateExistingEvent(Event event);

    public void deleteExistingEvent(Long eventId);
}
