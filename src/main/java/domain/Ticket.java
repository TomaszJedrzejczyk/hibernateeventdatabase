package domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Ticket {

    @Id
    private Long id;
    @ManyToOne
    private Event event;
    @ManyToOne
    private User user;
    private Integer seats;

}
