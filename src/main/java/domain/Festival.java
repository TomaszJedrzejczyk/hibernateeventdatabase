package domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Data
@ToString(callSuper = true)
@DiscriminatorValue("FESTIVAL")
public class Festival extends Event {
    private Integer lengthInDays;
    private String type;
}
