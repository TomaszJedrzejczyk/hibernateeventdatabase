package domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Data
@ToString(callSuper = true)
@DiscriminatorValue("CONCERT")
public class Concert extends Event {
    private String musicGenre;
    private String artist;

}
