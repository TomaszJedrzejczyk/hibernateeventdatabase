package domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@ToString(exclude = {"tickets", "events"})
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String email;
//    @Transient // dzieki temu hibernate nie bedzie mapowal tej czesci
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Ticket> tickets;

    @ManyToMany
    @JoinTable(
            name = "ticket",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "event_id")
    )
    private List<Event> events;

}
