package domain;

import java.util.List;

public interface UserRepository {
     User createNewUser(User user);

     User findUserByEmail(String email);

     List<Event> findAllEventsByUser(User user);
}
