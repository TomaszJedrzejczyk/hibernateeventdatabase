package domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

//@Data
@Getter
@Setter
@ToString(exclude = "users")
@Entity
@Table(name = "event") // nadajemy nazwe z tabeli
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)// kazda klasa będzie miała swoją oddzielną tabele
@DiscriminatorColumn(name = "event_type",discriminatorType = DiscriminatorType.STRING)
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)//->zapisuje tylk oevent a nie powiazane relacje, wszelie zmiany na eventach beda zapisywane w tabeli historie
@AuditTable("event_history")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Column(name = "date_time")
    private LocalDateTime date;
    @Column(name = "number_of_seats")
    private Integer numberOfSeats;
    @Column(name = "ticket_price") //ustawiamy jaka jest nazwa kolumny w naszej bazie danych
    private Double ticketPrice;
    @Version
    @Column(name = "version_lock")
    private Integer versionLock;
    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;
    @ManyToMany(mappedBy = "events")
    private List<User> users;
 }
