package domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@ToString(exclude = "events")
@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String city;
    private String name;

    @OneToMany(mappedBy = "location", fetch = FetchType.EAGER) // odnosimy sie w tym miejscu do location, to dotyczy sie tylko odczytowi
    private List<Event> events;
}
