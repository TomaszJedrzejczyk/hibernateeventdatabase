package domain;

import java.util.List;

public interface LocationRepository {

   public Location findLocationById(Long id);

   List<Event> findListOfEventsByLocation(Location location);
}
