import database.DataBaseFacade;
import database.JdbsEventRepository;
import database.HibernateConfiguration;
import database.JdbcTicketRepository;
import domain.Concert;
import domain.Event;
//import domain.Ticket;
import domain.EventRepository;
import domain.Festival;
import domain.Location;
import domain.LocationRepository;
import domain.Ticket;
import domain.User;
import domain.UserRepository;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final EventRepository eventRepository = DataBaseFacade.getEventRepository();
    private static final UserRepository userRepository = DataBaseFacade.getUserRepository();
    private static final LocationRepository locationRepository = DataBaseFacade.getLocationRepository();
//    private static final TicketRepository ticketRepository = new TicketRepository();
    //TODO implement buying tickets in hibernate repository

    public static void main(String[] args) {
        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

        EntityManager entityManager = HibernateConfiguration.getEntityMenager();

        enversGetRevision(entityManager);


        entityManager.close();
        HibernateConfiguration.close();
    }

    private static void enversExample(EntityManager entityManager) {
        Location location = new Location();
        location.setName("SDA");
        location.setCity("Łódź");

        Event event = new Event();
        event.setName("Test");
        event.setNumberOfSeats(15);
        event.setTicketPrice(12.5);
        event.setLocation(location);
        event.setDate(LocalDateTime.now());

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(location);
        entityManager.persist(event);
        transaction.commit();

        event.setName("Nowa nazwa");
        event.setTicketPrice(20.0);

        transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(event);
        transaction.commit();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        event.setTicketPrice(100.0);

        transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.merge(event);
        transaction.commit();
    }

    private static void enversGetRevision(EntityManager entityManager){
        AuditReader auditReader = AuditReaderFactory.get(entityManager);

        List<Number> revisions = auditReader.getRevisions(Event.class, 3L);
        revisions.forEach(revision-> {
            Event event = auditReader.find(Event.class, 3L, revision);
            System.out.println(event);
        });
    }

    private static void oneToManyExample(EntityManager entityManager) {
        Location location = locationRepository.findLocationById(1L);
        System.out.println(location);

        List<Event> eventsAtLocation = location.getEvents();
        eventsAtLocation.forEach(System.out::println);
        System.out.println("-----");
    }

    private static void basicOperationsExample(EntityManager entityManager) {
        List<Event> allEvents = eventRepository.findAllEvents();
        allEvents.forEach(System.out::println);

        User user = new User();
        user.setEmail("example@gmail.com");
        user.setPassword("secret-password");

        user = userRepository.createNewUser(user);
        System.out.println(user);

        Event event = allEvents.get(0);

        event.setName("Krzysztof Ibisz");
        event.setTicketPrice(1000.0);
        event.setNumberOfSeats(2);
        event.setDate(LocalDateTime.now().plusDays(1));

        event = eventRepository.createNewEvent(event);

        event = eventRepository.findEventById(event.getId());
        System.out.println(event);

        eventRepository.deleteExistingEvent(event.getId());

        allEvents = eventRepository.findAllEvents();
        allEvents.forEach(System.out::println);

        Ticket ticket = new Ticket();
        ticket.setEvent(allEvents.get(0));
        ticket.setUser(user);
        ticket.setSeats(5);

//        ticket = ticketRepository.buy(ticket);
        System.out.println(ticket);
    }

    private static void manyToManyExample(EntityManager entityManager) {
        User user = userRepository.findUserByEmail("piotrgajow@gmail.com");
        System.out.println(user);

        List<Event> eventsForUser = userRepository.findAllEventsByUser(user);

        eventsForUser.forEach(System.out::println);
    }

    private static void inheritanceExample(EntityManager entityManager) {
        Location location = new Location();
        location.setCity("Los Angeles");
        location.setName("Wytwórnia");

        Concert concert = new Concert();
        concert.setArtist("David Bowie");
        concert.setMusicGenre("RnB");
        concert.setDate(LocalDateTime.now().plusDays(2));
        concert.setName("De Best");
        concert.setNumberOfSeats(50);
        concert.setTicketPrice(5.0);
        concert.setLocation(location);

        Location location2 = new Location();
        location2.setCity("Gdynia");
        location2.setName("Psie Pole");

        Festival festival = new Festival();
        festival.setLengthInDays(3);
        festival.setType("Metalowo - filmowy");
        festival.setDate(LocalDateTime.now().minusMonths(1));
        festival.setName("Teownik");
        festival.setNumberOfSeats(5000);
        festival.setTicketPrice(2.5);
        festival.setLocation(location);

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(location);
        entityManager.persist(location2);
        entityManager.persist(festival);
        entityManager.persist(concert);
        transaction.commit();
    }

    private static void lockingExample(EntityManager entityManager) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj id wydarzenia: ");
        Long eventId = Long.valueOf(scanner.nextLine());

        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        Event event = entityManager.find(Event.class, eventId);

        System.out.println("Podaj nową nazwę dla wydarzenia - " + event.getName());
        String newName = scanner.nextLine();
        event.setName(newName);

        System.out.println("Zapisuję...");
        scanner.nextLine();
        entityManager.merge(event);
        transaction.commit();
    }

    private static void criteriaApiExample(EntityManager entityManager) {
        TypedQuery<Event> query;
//        query = entityManager.createQuery("from Event where ticketPrice = 0.0 OR numberOfSeats > 100", Event.class);

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Event> criteriaQuery = criteriaBuilder.createQuery(Event.class);
        Root<Event> root = criteriaQuery.from(Event.class);

        Expression<Double> ticketPrice = root.get("ticketPrice");
        Expression<Integer> numberOfSeats = root.get("numberOfSeats");
        Expression<Integer> numberOfSeatsParam = criteriaBuilder.parameter(Integer.class, "nos");

        Predicate ticketPriceIsZero = criteriaBuilder.equal(ticketPrice, 0.0);
        Predicate numberOfSeatsGreaterThan =
                criteriaBuilder.greaterThan(numberOfSeats, numberOfSeatsParam);

        criteriaQuery.select(root).where(
                criteriaBuilder.or(ticketPriceIsZero, numberOfSeatsGreaterThan)
        );

        query = entityManager.createQuery(criteriaQuery);
        query.setParameter("nos", 100);

        List<Event> foundEvents = query.getResultList();
        foundEvents.forEach(System.out::println);
    }

}